<?php

/**
 * @file
 * Feeds hooks and callbacks.
 */

/**
 * Implementaion of hook_feeds_processor_targets_alter().
 */
function feeds_empty_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  foreach ($targets as $target => &$info) {
    if (!empty($info['post_process']) && in_array('feeds_filter_empty_field_items', $info['post_process'])) {

      $info['post_process'][] = 'feeds_empty_post_process_callback';

      if (isset($info['form_callback'])) {
        $info['feeds_empty_old_form_callback'] = $info['form_callback'];
      }
      if (isset($info['summary_callback'])) {
        $info['feeds_empty_old_summary_callback'] = $info['summary_callback'];
      }

      $info['form_callback'] = 'feeds_empty_form_callback';
      $info['summary_callback'] = 'feeds_empty_summary_callback';
    }
  }
}

/**
 * Mapping summary callback.
 */
function feeds_empty_summary_callback($mapping, $target, $form, $form_state) {
  $summary = t('Reuse old values if empty: ');
  if (!empty($mapping['feeds_empty_use_old'])) {
    $summary .= t('<strong>Yes</strong>');
  }
  else {
    $summary .= t('<strong>No</strong>');
  }

  if (isset($target['feeds_empty_old_summary_callback'])) {
    $summary = $summary . '<br />' . $target['feeds_empty_old_summary_callback']($mapping, $target, $form, $form_state);
  }

  return $summary;
}

/**
 * Mapping form callback.
 */
function feeds_empty_form_callback($mapping, $target, $form, $form_state) {

  $mapping_form = array();

  $mapping_form['feeds_empty_use_old'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use old value if empty'),
    '#default_value' => !empty($mapping['feeds_empty_use_old']),
  );

  if (isset($target['feeds_empty_old_form_callback'])) {
    $mapping_form += $target['feeds_empty_old_form_callback']($mapping, $target, $form, $form_state);
  }

  return $mapping_form;
}

/**
 * Post-process callback.
 *
 * Add existing values if the field is empty, and configured to do so.
 */
function feeds_empty_post_process_callback(FeedsSource $source, $entity, $target, array $target_info, array $mapping) {
  // We haven't been configured to run.
  if (empty($mapping['feeds_empty_use_old'])) {
    return;
  }

  $field_name = $target;
  if (!empty($target_info['real_target'])) {
    $field_name = $target_info['real_target'];
  }

  if (empty($entitiy->$field_name) || empty($entitiy->{$field_name}['und'])) {
    $entity_type = $source->importer->processor->entityType();
    list($id, , ) = entity_extract_ids($entity_type, $entity);

    if ($id) {
      $old_entity = entity_load_unchanged($entity_type, $id);
      $entity->$field_name = $old_entity->$field_name;
    }
  }
}
